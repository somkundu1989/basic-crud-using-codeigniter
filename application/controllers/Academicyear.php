<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Academicyear extends CI_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Common', 'commonmodel');	
	}
	public function index()
	{

		$sql = "SELECT * FROM academic_years WHERE deleted_at IS NULL";	
		$allRecords = $this->commonmodel->get_records_from_sql($sql);
		$data = null;
		if($_GET){
			if($_GET['action']=='delete'){

				$sql = "DELETE FROM academic_years WHERE id=".$_GET['id'];
				$this->commonmodel->put_record_by_sql($sql);
				redirect('/academicyear');
			}
			if($_GET['action']=='edit'){
				$sql = "SELECT * FROM academic_years WHERE id=".$_GET['id'];
				$data = $this->commonmodel->get_records_from_sql($sql);
				if(!empty($data)){
					$data=$data[0];
				}
			}
		}
		if($_POST){
			$sql = "INSERT INTO academic_years(from_date, to_date, title, slug) VALUES ('".$_POST['from_date']."', '".$_POST['to_date']."', '".$_POST['title']."', '".$_POST['slug']."')";
			if($_POST['record_id']>0){
				$sql = "UPDATE academic_years SET from_date='".$_POST['from_date']."',to_date='".$_POST['to_date']."',title='".$_POST['title']."',slug='".$_POST['slug']."'  WHERE id=".$_POST['record_id'];
			}
			$this->commonmodel->put_record_by_sql($sql);
			redirect('/academicyear');
		}

		$this->load->view('academicyear/index',['allRecords'=>$allRecords, 'data'=>$data]);
	}
}
