<?php $this->load->view('layout/header'); ?>
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Academic Years</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item active">Academic Years</li>
              <?php if(!empty($_GET) && ($_GET['action']=='add' || $_GET['action']=='edit')){ ?>
        <li class="breadcrumb-item active"><?=$_GET['action']?> Academic Years</li>
        <?php } ?>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">


    <?php if(!empty($_GET) && ($_GET['action']=='add' || $_GET['action']=='edit')){ ?>
      <?php $this->load->view('academicyear/addoredit'); ?>
    <?php } ?>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Academic Years</h3>

                <div class="card-tools">
                  <a href="<?=base_url()?>academicyear?action=add" class="btn btn-success float-right"><i class="fas fa-plus"></i> Create New Academic Year</a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>From Date</th>
                      <th>To Date</th>
                      <th>Title</th>
                      <th>Current Session</th>
                      <th>Next Session</th>
                      <th colspan="2"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($allRecords) { ?>
                    <?php foreach ($allRecords as $obj) { ?>
                    <tr>
                      <td><?=$obj->from_date?></td>
                      <td><?=$obj->to_date?></td>
                      <td><?=$obj->title?></td>
                      <td><?=($obj->is_activate?'Active':'Inactive')?></td>
                      <td><?=($obj->is_next==1?'Active':'Inactive')?></td>
                      <td><a href="?id=<?=$obj->id?>&action=edit" class="btn btn-success">edit</a></td>
                      <td><a href="?id=<?=$obj->id?>&action=delete" class="btn btn-danger">delete</a></td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                      <td colspan="7">No Data Found</td>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
<?php $this->load->view('layout/footer'); ?>